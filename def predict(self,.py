def predict(self,
            images,
            draw_threshold=0.5,
            output_dir='output',
            save_results=False,
            visualize=True):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    self.dataset.set_images(images)
    loader = create('TestReader')(self.dataset, 0)

    imid2path = self.dataset.get_imid2path()

    def setup_metrics_for_loader():
        # mem
        metrics = copy.deepcopy(self._metrics)
        mode = self.mode
        save_prediction_only = self.cfg[
            'save_prediction_only'] if 'save_prediction_only' in self.cfg else None
        output_eval = self.cfg[
            'output_eval'] if 'output_eval' in self.cfg else None

        # modify
        self.mode = '_test'
        self.cfg['save_prediction_only'] = True
        self.cfg['output_eval'] = output_dir
        self.cfg['imid2path'] = imid2path
        self._init_metrics()

        # restore
        self.mode = mode
        self.cfg.pop('save_prediction_only')
        if save_prediction_only is not None:
            self.cfg['save_prediction_only'] = save_prediction_only

        self.cfg.pop('output_eval')
        if output_eval is not None:
            self.cfg['output_eval'] = output_eval

        self.cfg.pop('imid2path')

        _metrics = copy.deepcopy(self._metrics)
        self._metrics = metrics

        return _metrics

    if save_results:
        metrics = setup_metrics_for_loader()
    else:
        metrics = []

    anno_file = self.dataset.get_anno()
    clsid2catid, catid2name = get_categories(
        self.cfg.metric, anno_file=anno_file)

    # Run Infer 
    self.status['mode'] = 'test'
    self.model.eval()
    if self.cfg.get('print_flops', False):
        flops_loader = create('TestReader')(self.dataset, 0)
        self._flops(flops_loader)
    results = []
    for step_id, data in enumerate(tqdm(loader)):
        self.status['step_id'] = step_id
        # forward
        outs = self.model(data)
        # print(str(data))

        for _m in metrics:
            _m.update(data, outs)

        for key in ['im_shape', 'scale_factor', 'im_id']:
            if isinstance(data, typing.Sequence):
                outs[key] = data[0][key]
            else:
                outs[key] = data[key]
        for key, value in outs.items():
            if hasattr(value, 'numpy'):
                outs[key] = value.numpy()
        results.append(outs)
        # print(str(outs))

    # sniper
    if type(self.dataset) == SniperCOCODataSet:
        results = self.dataset.anno_cropper.aggregate_chips_detections(
            results)

    for _m in metrics:
        _m.accumulate()
        _m.reset()

    if visualize:
        for outs in results:
            batch_res = get_infer_results(outs, clsid2catid)
            bbox_num = outs['bbox_num']
            logger.info(str(batch_res))

            start = 0
            for i, im_id in enumerate(outs['im_id']):
                image_path = imid2path[int(im_id)]
                image = Image.open(image_path).convert('RGB')
                image = ImageOps.exif_transpose(image)
                self.status['original_image'] = np.array(image.copy())

                end = start + bbox_num[i]
                bbox_res = batch_res['bbox'][start:end] \
                        if 'bbox' in batch_res else None
                # print(bbox_res)
                print(str(batch_res))
                print("1030")
                mask_res = batch_res['mask'][start:end] \
                        if 'mask' in batch_res else None
                segm_res = batch_res['segm'][start:end] \
                        if 'segm' in batch_res else None
                keypoint_res = batch_res['keypoint'][start:end] \
                        if 'keypoint' in batch_res else None
                pose3d_res = batch_res['pose3d'][start:end] \
                        if 'pose3d' in batch_res else None
                # image = visualize_results(
                #     image, bbox_res, mask_res, segm_res, keypoint_res,
                #     pose3d_res, int(im_id), catid2name, draw_threshold)
                # self.status['result_image'] = np.array(image.copy())
                # if self._compose_callback:
                #     self._compose_callback.on_step_end(self.status)
                # save image with detection
                # save_name = self._get_save_image_name(output_dir,
                #                                       image_path)
                # logger.info("Detection bbox results save in {}".format(
                #     save_name))
                # image.save(save_name, quality=95)

                start = end
    return results

