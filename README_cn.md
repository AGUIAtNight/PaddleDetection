
有原生绘图算法draw-bbox,可以提取出点位


采用蒙版进行操作，得到所需区域



![输入图片说明](1710146200217.png)



只保留!![输入图片说明](1710146346331.png)下的bbox部分并返回，或添加一个新训练函数




![输入图片说明](1710145784237.png)

bbox确实是对应检测区域，但是有很多杂质框


python tools/infer.py -c configs/ppyolo/ppyolo_r50vd_dcn_1x_coco.yml -o use_gpu=false weights=https://paddledet.bj.bcebos.com/models/ppyolo_r50vd_dcn_1x_coco.pdparams --infer_img=demo/000000014439.jpg



category_id是标签
可通过bbox计算出形心，简化为cbox和category_id，并生成相对向量列表，进行视差识别


运行输出

bbox_res = batch_res['bbox'][start:end] \
                            if 'bbox' in batch_res else None


batch_res结果

INFO: {'bbox': [{'image_id': 0, 'category_id': 1, 'bbox': [163.01693725585938, 81.64559936523438, 35.227386474609375, 85.74794006347656], 'score': 0.9523873329162598}, {'image_id': 0, 'category_id': 1, 'bbox': [103.69308471679688, 45.28235626220703, 22.778152465820312, 47.06730651855469], 'score': 0.9486417770385742}, {'image_id': 0, 'category_id': 1, 'bbox': [410.77569580078125, 84.6987075805664, 91.27398681640625, 201.3977279663086], 'score': 0.9311496019363403}, {'image_id': 0, 'category_id': 1, 'bbox': [580.3850708007812, 113.47499084472656, 31.0224609375, 87.43368530273438], 'score': 0.8681186437606812}, {'image_id': 0, 'category_id': 62, 'bbox': [74.17538452148438, 121.53815460205078, 28.30572509765625, 31.582801818847656], 'score': 0.8621149659156799}, {'image_id': 0, 'category_id': 38, 'bbox': [158.66375732421875, 99.47403717041016, 458.41119384765625, 241.31877899169922], 'score': 0.8587828874588013}, {'image_id': 0, 'category_id': 1, 'bbox': [267.1729431152344, 84.55514526367188, 24.984375, 82.66239929199219], 'score': 0.8221476078033447}, {'image_id': 0, 'category_id': 1, 'bbox': [348.5954284667969, 43.863433837890625, 15.76409912109375, 54.08282470703125], 'score': 0.7898276448249817}, {'image_id': 0, 'category_id': 1, 'bbox': [506.553955078125, 115.50984191894531, 88.821044921875, 157.02659606933594], 'score': 0.7674617171287537}, {'image_id': 0, 'category_id': 1, 'bbox': [328.97283935546875, 38.41905975341797, 16.97052001953125, 41.15814208984375], 'score': 0.7647908926010132}, {'image_id': 0, 'category_id': 1, 'bbox': [169.35107421875, 47.09164047241211, 8.51129150390625, 12.991142272949219], 'score': 0.6630620360374451}, {'image_id': 0, 'category_id': 1, 'bbox': [364.0526123046875, 57.54597091674805, 16.26666259765625, 50.56865310668945], 'score': 0.6352982521057129}, {'image_id': 0, 'category_id': 1, 'bbox': [27.252973556518555, 117.72573852539062, 32.67363929748535, 34.914154052734375], 'score': 0.6338553428649902}, {'image_id': 0, 'category_id': 1, 'bbox': [378.2804870605469, 39.14801788330078, 15.9246826171875, 44.58753204345703], 'score': 0.6177567839622498}, {'image_id': 0, 'category_id': 1, 'bbox': [186.37814331054688, 44.481842041015625, 12.606231689453125, 15.299453735351562], 'score': 0.5253617763519287}, {'image_id': 0, 'category_id': 62, 'bbox': [98.57674407958984, 130.52426147460938, 17.056411743164062, 23.995819091796875], 'score': 0.42762550711631775}, {'image_id': 0, 'category_id': 27, 'bbox': [1.9103202819824219, 150.37460327148438, 34.68462371826172, 21.637664794921875], 'score': 0.3406195342540741}, {'image_id': 0, 'category_id': 27, 'bbox': [100.22119140625, 153.69656372070312, 17.407272338867188, 13.561187744140625], 'score': 0.28557664155960083}, {'image_id': 0, 'category_id': 1, 'bbox': [464.5977783203125, 15.314728736877441, 6.245361328125, 17.570868492126465], 'score': 0.2770850658416748}, {'image_id': 0, 'category_id': 27, 'bbox': [65.50455474853516, 135.86273193359375, 17.784088134765625, 17.833526611328125], 'score': 0.27335259318351746}, {'image_id': 0, 'category_id': 1, 'bbox': [278.1131896972656, 79.93157196044922, 19.128173828125, 88.01862335205078], 'score': 0.25723665952682495}, {'image_id': 0, 'category_id': 27, 'bbox': [55.612030029296875, 152.50001525878906, 44.405792236328125, 20.424102783203125], 'score': 0.24083048105239868}, {'image_id': 0, 'category_id': 1, 'bbox': [279.1971130371094, 80.64604187011719, 16.4736328125, 27.218643188476562], 'score': 0.21805967390537262}, {'image_id': 0, 'category_id': 1, 'bbox': [504.3023376464844, 114.20023345947266, 49.506561279296875, 63.160621643066406], 'score': 0.19797641038894653}, {'image_id': 0, 'category_id': 1, 'bbox': [265.62255859375, 82.0456314086914, 33.19903564453125, 87.26961517333984], 'score': 0.18388086557388306}, {'image_id': 0, 'category_id': 27, 'bbox': [84.91261291503906, 155.86294555664062, 17.432418823242188, 15.49658203125], 'score': 0.1714654117822647}, {'image_id': 0, 'category_id': 47, 'bbox': [508.8377990722656, 274.14990234375, 10.541534423828125, 8.673828125], 'score': 0.1682482808828354}, {'image_id': 0, 'category_id': 62, 'bbox': [27.252973556518555, 117.72573852539062, 32.67363929748535, 34.914154052734375], 'score': 0.15387071669101715}, {'image_id': 0, 'category_id': 3, 'bbox': [621.4003295898438, 5.1622514724731445, 15.651611328125, 4.050973892211914], 'score': 0.15260817110538483}, {'image_id': 0, 'category_id': 1, 'bbox': [239.8620147705078, 95.60108947753906, 12.262298583984375, 19.933425903320312], 'score': 0.12946726381778717}, {'image_id': 0, 'category_id': 1, 'bbox': [267.5434265136719, 88.84803771972656, 28.40875244140625, 79.33831787109375], 'score': 0.12095747888088226}, {'image_id': 0, 'category_id': 1, 'bbox': [265.1093444824219, 86.20228576660156, 33.6033935546875, 84.263671875], 'score': 0.11267218738794327}, {'image_id': 0, 'category_id': 62, 'bbox': [96.5557861328125, 128.7020263671875, 22.073577880859375, 33.7235107421875], 'score': 0.11178295314311981}, {'image_id': 0, 'category_id': 1, 'bbox': [582.9432373046875, 113.26557922363281, 28.5562744140625, 62.271728515625], 'score': 0.10880493372678757}, {'image_id': 0, 'category_id': 3, 'bbox': [0.0, 16.530506134033203, 14.529098510742188, 7.268653869628906], 'score': 0.10859532654285431}, {'image_id': 0, 'category_id': 38, 'bbox': [368.067138671875, 165.562744140625, 243.11602783203125, 38.471893310546875], 'score': 0.10563790053129196}, {'image_id': 0, 'category_id': 1, 'bbox': [364.6383361816406, 57.85504150390625, 16.05401611328125, 56.67082214355469], 'score': 0.09884529560804367}, {'image_id': 0, 'category_id': 1, 'bbox': [57.85503005981445, 137.07394409179688, 41.07229232788086, 35.8179931640625], 'score': 0.09510890394449234}, {'image_id': 0, 'category_id': 62, 'bbox': [74.34246826171875, 122.50325012207031, 29.8848876953125, 32.878509521484375], 'score': 0.09373864531517029}, {'image_id': 0, 'category_id': 34, 'bbox': [348.4073791503906, 62.41240310668945, 7.1961669921875, 5.048358917236328], 'score': 0.0937190055847168}, {'image_id': 0, 'category_id': 38, 'bbox': [157.13185119628906, 87.8681869506836, 461.98387145996094, 238.69379425048828], 'score': 0.09282081574201584}, {'image_id': 0, 'category_id': 1, 'bbox': [505.52392578125, 115.86605072021484, 56.78759765625, 155.9321060180664], 'score': 0.09127474576234818}, {'image_id': 0, 'category_id': 62, 'bbox': [20.605754852294922, 135.21688842773438, 16.9403076171875, 15.247650146484375], 'score': 0.08903761208057404}, {'image_id': 0, 'category_id': 27, 'bbox': [4.236656188964844, 135.34336853027344, 31.969482421875, 17.5994873046875], 'score': 0.08678808808326721}, {'image_id': 0, 'category_id': 1, 'bbox': [379.8614807128906, 40.56462860107422, 13.759033203125, 40.9747314453125], 'score': 0.08356528729200363}, {'image_id': 0, 'category_id': 31, 'bbox': [84.91261291503906, 155.86294555664062, 17.432418823242188, 15.49658203125], 'score': 0.083266980946064}, {'image_id': 0, 'category_id': 1, 'bbox': [582.832275390625, 111.97151184082031, 28.1142578125, 89.05728149414062], 'score': 0.08207747340202332}, {'image_id': 0, 'category_id': 1, 'bbox': [410.2742614746094, 18.803682327270508, 6.53497314453125, 14.134885787963867], 'score': 0.08069997280836105}, {'image_id': 0, 'category_id': 38, 'bbox': [348.4073791503906, 62.41240310668945, 7.1961669921875, 5.048358917236328], 'score': 0.0804661437869072}, {'image_id': 0, 'category_id': 3, 'bbox': [491.3437805175781, 0.6753189563751221, 15.003662109375, 6.461724042892456], 'score': 0.07974940538406372}, {'image_id': 0, 'category_id': 1, 'bbox': [155.0016632080078, 121.06098937988281, 20.50732421875, 41.339324951171875], 'score': 0.0754125565290451}, {'image_id': 0, 'category_id': 51, 'bbox': [508.8377990722656, 274.14990234375, 10.541534423828125, 8.673828125], 'score': 0.06950979679822922}, {'image_id': 0, 'category_id': 1, 'bbox': [279.9168395996094, 80.5907211303711, 16.9473876953125, 42.87054443359375], 'score': 0.06857393682003021}, {'image_id': 0, 'category_id': 1, 'bbox': [411.8911437988281, 88.08795928955078, 85.0897216796875, 210.59477996826172], 'score': 0.06644309312105179}, {'image_id': 0, 'category_id': 27, 'bbox': [57.3817253112793, 152.8783416748047, 33.809452056884766, 19.464996337890625], 'score': 0.06579175591468811}, {'image_id': 0, 'category_id': 15, 'bbox': [541.5082397460938, 16.375507354736328, 26.3614501953125, 12.74056625366211], 'score': 0.06562618166208267}, {'image_id': 0, 'category_id': 1, 'bbox': [417.2962646484375, 87.95060729980469, 66.63714599609375, 197.25672912597656], 'score': 0.0627821683883667}, {'image_id': 0, 'category_id': 1, 'bbox': [187.15985107421875, 44.91228103637695, 10.810028076171875, 14.471435546875], 'score': 0.062317974865436554}, {'image_id': 0, 'category_id': 38, 'bbox': [159.93609619140625, 87.80223083496094, 460.067626953125, 193.3937530517578], 'score': 0.06120152026414871}, {'image_id': 0, 'category_id': 1, 'bbox': [166.17410278320312, 82.07062530517578, 29.700042724609375, 82.0453109741211], 'score': 0.060840312391519547}, {'image_id': 0, 'category_id': 3, 'bbox': [605.6505126953125, 5.365048885345459, 13.7423095703125, 3.709738254547119], 'score': 0.059054549783468246}, {'image_id': 0, 'category_id': 1, 'bbox': [0.0, 126.04899597167969, 8.50353717803955, 21.224853515625], 'score': 0.057227183133363724}, {'image_id': 0, 'category_id': 1, 'bbox': [508.2998962402344, 118.50183868408203, 74.88406372070312, 152.47808074951172], 'score': 0.05692616105079651}, {'image_id': 0, 'category_id': 15, 'bbox': [497.66845703125, 25.976497650146484, 20.5062255859375, 4.692436218261719], 'score': 0.055760178714990616}, {'image_id': 0, 'category_id': 1, 'bbox': [279.9592590332031, 79.89441680908203, 15.912353515625, 34.301666259765625], 'score': 0.05560290440917015}, {'image_id': 0, 'category_id': 1, 'bbox': [280.04949951171875, 79.7703628540039, 16.49560546875, 65.6472396850586], 'score': 0.05517078563570976}, {'image_id': 0, 'category_id': 27, 'bbox': [20.918540954589844, 135.88543701171875, 17.003955841064453, 16.297576904296875], 'score': 0.05394516885280609}, {'image_id': 0, 'category_id': 3, 'bbox': [424.52667236328125, 0.0, 15.063232421875, 4.050658702850342], 'score': 0.0526321679353714}, {'image_id': 0, 'category_id': 1, 'bbox': [352.3342590332031, 44.0297737121582, 12.76641845703125, 35.6136589050293], 'score': 0.0523824617266655}, {'image_id': 0, 'category_id': 1, 'bbox': [506.7310485839844, 107.9038314819336, 92.20034790039062, 163.54276275634766], 'score': 0.05208742991089821}, {'image_id': 0, 'category_id': 1, 'bbox': [379.8673400878906, 39.31773376464844, 15.396484375, 44.507110595703125], 'score': 0.05184461921453476}, {'image_id': 0, 'category_id': 62, 'bbox': [156.88832092285156, 123.81497192382812, 14.235809326171875, 33.384307861328125], 'score': 0.0501168929040432}, {'image_id': 0, 'category_id': 1, 'bbox': [505.5058288574219, 110.955322265625, 53.936309814453125, 157.12835693359375], 'score': 0.05004994943737984}, {'image_id': 0, 'category_id': 27, 'bbox': [2.7938013076782227, 151.2257843017578, 22.657950401306152, 20.4300537109375], 'score': 0.049837954342365265}, {'image_id': 0, 'category_id': 31, 'bbox': [1.9103202819824219, 150.37460327148438, 34.68462371826172, 21.637664794921875], 'score': 0.049077942967414856}, {'image_id': 0, 'category_id': 1, 'bbox': [349.57623291015625, 51.462791442871094, 17.8818359375, 48.344207763671875], 'score': 0.04811444133520126}, {'image_id': 0, 'category_id': 1, 'bbox': [327.77239990234375, 38.276702880859375, 17.759521484375, 41.243316650390625], 'score': 0.04789476469159126}, {'image_id': 0, 'category_id': 37, 'bbox': [325.9461364746094, 168.7016143798828, 5.48187255859375, 3.45587158203125], 'score': 0.04785195738077164}, {'image_id': 0, 'category_id': 1, 'bbox': [352.0867614746094, 50.1259765625, 13.36395263671875, 42.84584045410156], 'score': 0.04734914377331734}, {'image_id': 0, 'category_id': 27, 'bbox': [81.9166030883789, 152.35939025878906, 20.327438354492188, 18.96734619140625], 'score': 0.0469079427421093}, {'image_id': 0, 'category_id': 62, 'bbox': [37.21086120605469, 131.02786254882812, 20.57044219970703, 21.203369140625], 'score': 0.04667816683650017}, {'image_id': 0, 'category_id': 27, 'bbox': [58.027645111083984, 153.0387420654297, 22.23727035522461, 19.207244873046875], 'score': 0.04491680487990379}, {'image_id': 0, 'category_id': 1, 'bbox': [478.9885559082031, 23.081398010253906, 5.9256591796875, 9.550769805908203], 'score': 0.0445345938205719}, {'image_id': 0, 'category_id': 1, 'bbox': [422.13385009765625, 88.64215850830078, 70.02093505859375, 194.1235580444336], 'score': 0.042308442294597626}, {'image_id': 0, 'category_id': 31, 'bbox': [101.58711242675781, 153.90850830078125, 16.735153198242188, 13.288787841796875], 'score': 0.04149223491549492}, {'image_id': 0, 'category_id': 1, 'bbox': [265.23681640625, 87.43470001220703, 19.815185546875, 49.67278289794922], 'score': 0.0410604290664196}, {'image_id': 0, 'category_id': 1, 'bbox': [226.1794891357422, 83.91767883300781, 25.699676513671875, 33.74407958984375], 'score': 0.03906938433647156}, {'image_id': 0, 'category_id': 77, 'bbox': [504.02099609375, 144.37136840820312, 4.269775390625, 3.87969970703125], 'score': 0.038799580186605453}, {'image_id': 0, 'category_id': 1, 'bbox': [195.7422332763672, 124.55506134033203, 15.47125244140625, 31.711387634277344], 'score': 0.03787028416991234}, {'image_id': 0, 'category_id': 62, 'bbox': [1.5969181060791016, 130.3656768798828, 35.916120529174805, 25.72967529296875], 'score': 0.037462059408426285}, {'image_id': 0, 'category_id': 38, 'bbox': [361.32470703125, 95.989013671875, 144.992431640625, 176.43984985351562], 'score': 0.03709067776799202}, {'image_id': 0, 'category_id': 1, 'bbox': [398.39410400390625, 25.0598087310791, 6.01177978515625, 7.866392135620117], 'score': 0.03638140484690666}, {'image_id': 0, 'category_id': 1, 'bbox': [279.502685546875, 81.48350524902344, 7.65167236328125, 12.01300048828125], 'score': 0.03631816431879997}, {'image_id': 0, 'category_id': 62, 'bbox': [38.010623931884766, 137.3284454345703, 19.57343292236328, 14.631103515625], 'score': 0.035624489188194275}, {'image_id': 0, 'category_id': 62, 'bbox': [76.47907257080078, 122.70376586914062, 21.016143798828125, 27.88629150390625], 'score': 0.0351509228348732}, {'image_id': 0, 'category_id': 15, 'bbox': [305.0141906738281, 26.077545166015625, 21.137939453125, 4.832714080810547], 'score': 0.03484610840678215}, {'image_id': 0, 'category_id': 62, 'bbox': [98.63943481445312, 131.12774658203125, 17.366119384765625, 26.10107421875], 'score': 0.03460155054926872}, {'image_id': 0, 'category_id': 27, 'bbox': [21.948631286621094, 152.27066040039062, 15.285148620605469, 18.331695556640625], 'score': 0.03453721106052399}, {'image_id': 0, 'category_id': 1, 'bbox': [328.5123596191406, 38.36920928955078, 15.7481689453125, 38.360687255859375], 'score': 0.03412200137972832}, {'image_id': 0, 'category_id': 38, 'bbox': [415.9790344238281, 87.17354583740234, 127.19503784179688, 194.19498443603516], 'score': 0.0334664024412632}]}




绘制
for i, im_id in enumerate(outs['im_id']):
                    image_path = imid2path[int(im_id)]
                    image = Image.open(image_path).convert('RGB')
                    image = ImageOps.exif_transpose(image)
                    self.status['original_image'] = np.array(image.copy())

                    end = start + bbox_num[i]
                    bbox_res = batch_res['bbox'][start:end] \
                            if 'bbox' in batch_res else None
                    mask_res = batch_res['mask'][start:end] \
                            if 'mask' in batch_res else None
                    segm_res = batch_res['segm'][start:end] \
                            if 'segm' in batch_res else None
                    keypoint_res = batch_res['keypoint'][start:end] \
                            if 'keypoint' in batch_res else None
                    pose3d_res = batch_res['pose3d'][start:end] \
                            if 'pose3d' in batch_res else None
                    image = visualize_results(
                        image, bbox_res, mask_res, segm_res, keypoint_res,
                        pose3d_res, int(im_id), catid2name, draw_threshold)


ppdet >engine> trainer.py >Trainer >predict存在结果绘制



简体中文 | [English](README_en.md)

<div align="center">
<p align="center">
  <img src="https://user-images.githubusercontent.com/48054808/160532560-34cf7a1f-d950-435e-90d2-4b0a679e5119.png" align="middle" width = "800" />
</p>

<p align="center">
    <a href="./LICENSE"><img src="https://img.shields.io/badge/license-Apache%202-dfd.svg"></a>
    <a href="https://github.com/PaddlePaddle/PaddleDetection/releases"><img src="https://img.shields.io/github/v/release/PaddlePaddle/PaddleDetection?color=ffa"></a>
    <a href=""><img src="https://img.shields.io/badge/python-3.7+-aff.svg"></a>
    <a href=""><img src="https://img.shields.io/badge/os-linux%2C%20win%2C%20mac-pink.svg"></a>
    <a href="https://github.com/PaddlePaddle/PaddleDetection/stargazers"><img src="https://img.shields.io/github/stars/PaddlePaddle/PaddleDetection?color=ccf"></a>
</p>
</div>

## 💌目录
- [💌目录](#目录)
- [🌈简介](#简介)
- [📣最新进展](#最新进展)
- [👫开源社区](#开源社区)
- [✨主要特性](#主要特性)
    - [🧩模块化设计](#模块化设计)
    - [📱丰富的模型库](#丰富的模型库)
    - [🎗️产业特色模型|产业工具](#️产业特色模型产业工具)
    - [💡🏆产业级部署实践](#产业级部署实践)
- [🍱安装](#安装)
- [🔥教程](#教程)
- [🔑FAQ](#faq)
- [🧩模块组件](#模块组件)
- [📱模型库](#模型库)
- [⚖️模型性能对比](#️模型性能对比)
    - [🖥️服务器端模型性能对比](#️服务器端模型性能对比)
    - [⌚️移动端模型性能对比](#️移动端模型性能对比)
- [🎗️产业特色模型|产业工具](#️产业特色模型产业工具-1)
  - [💎PP-YOLOE 高精度目标检测模型](#pp-yoloe-高精度目标检测模型)
  - [💎PP-YOLOE-R 高性能旋转框检测模型](#pp-yoloe-r-高性能旋转框检测模型)
  - [💎PP-YOLOE-SOD 高精度小目标检测模型](#pp-yoloe-sod-高精度小目标检测模型)
  - [💫PP-PicoDet 超轻量实时目标检测模型](#pp-picodet-超轻量实时目标检测模型)
  - [📡PP-Tracking 实时多目标跟踪系统](#pp-tracking-实时多目标跟踪系统)
  - [⛷️PP-TinyPose 人体骨骼关键点识别](#️pp-tinypose-人体骨骼关键点识别)
  - [🏃🏻PP-Human 实时行人分析工具](#pp-human-实时行人分析工具)
  - [🏎️PP-Vehicle 实时车辆分析工具](#️pp-vehicle-实时车辆分析工具)
- [💡产业实践范例](#产业实践范例)
- [🏆企业应用案例](#企业应用案例)
- [📝许可证书](#许可证书)
- [📌引用](#引用)


## 🌈简介

PaddleDetection是一个基于PaddlePaddle的目标检测端到端开发套件，在提供丰富的模型组件和测试基准的同时，注重端到端的产业落地应用，通过打造产业级特色模型|工具、建设产业应用范例等手段，帮助开发者实现数据准备、模型选型、模型训练、模型部署的全流程打通，快速进行落地应用。

主要模型效果示例如下（点击标题可快速跳转）：

|                                                  [**通用目标检测**](#pp-yoloe-高精度目标检测模型)                                                  |                                                [**小目标检测**](#pp-yoloe-sod-高精度小目标检测模型)                                                |                                                  [**旋转框检测**](#pp-yoloe-r-高性能旋转框检测模型)                                                  |                                            [**3D目标物检测**](https://github.com/PaddlePaddle/Paddle3D)                                            |
| :--------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------------: |
| <img src='https://user-images.githubusercontent.com/61035602/206095864-f174835d-4e9a-42f7-96b8-d684fc3a3687.png' height="126px" width="180px"> | <img src='https://user-images.githubusercontent.com/61035602/206095892-934be83a-f869-4a31-8e52-1074184149d1.jpg' height="126px" width="180px"> |  <img src='https://user-images.githubusercontent.com/61035602/206111796-d9a9702a-c1a0-4647-b8e9-3e1307e9d34c.png' height="126px" width="180px">  | <img src='https://user-images.githubusercontent.com/61035602/206095622-cf6dbd26-5515-472f-9451-b39bbef5b1bf.gif' height="126px" width="180px"> |
|                                                              [**人脸检测**](#模型库)                                                               |                                                [**2D关键点检测**](#️pp-tinypose-人体骨骼关键点识别)                                                 |                                                  [**多目标追踪**](#pp-tracking-实时多目标跟踪系统)                                                   |                                                              [**实例分割**](#模型库)                                                               |
| <img src='https://user-images.githubusercontent.com/61035602/206095684-72f42233-c9c7-4bd8-9195-e34859bd08bf.jpg' height="126px" width="180px"> | <img src='https://user-images.githubusercontent.com/61035602/206100220-ab01d347-9ff9-4f17-9718-290ec14d4205.gif' height="126px" width="180px"> | <img src='https://user-images.githubusercontent.com/61035602/206111753-836e7827-968e-4c80-92ef-7a78766892fc.gif' height="126px" width="180px"  > | <img src='https://user-images.githubusercontent.com/61035602/206095831-cc439557-1a23-4a99-b6b0-b6f2e97e8c57.jpg' height="126px" width="180px"> |
|                                               [**车辆分析——车牌识别**](#️pp-vehicle-实时车辆分析工具)                                               |                                               [**车辆分析——车流统计**](#️pp-vehicle-实时车辆分析工具)                                               |                                                [**车辆分析——违章检测**](#️pp-vehicle-实时车辆分析工具)                                                |                                               [**车辆分析——属性分析**](#️pp-vehicle-实时车辆分析工具)                                               |
| <img src='https://user-images.githubusercontent.com/61035602/206099328-2a1559e0-3b48-4424-9bad-d68f9ba5ba65.gif' height="126px" width="180px"> | <img src='https://user-images.githubusercontent.com/61035602/206095918-d0e7ad87-7bbb-40f1-bcc1-37844e2271ff.gif' height="126px" width="180px"> | <img src='https://user-images.githubusercontent.com/61035602/206100295-7762e1ab-ffce-44fb-b69d-45fb93657fa0.gif' height="126px" width="180px"  > | <img src='https://user-images.githubusercontent.com/61035602/206095905-8255776a-d8e6-4af1-b6e9-8d9f97e5059d.gif' height="126px" width="180px"> |
|                                                [**行人分析——闯入分析**](#pp-human-实时行人分析工具)                                                |                                                [**行人分析——行为分析**](#pp-human-实时行人分析工具)                                                |                                                 [**行人分析——属性分析**](#pp-human-实时行人分析工具)                                                 |                                                [**行人分析——人流统计**](#pp-human-实时行人分析工具)                                                |
| <img src='https://user-images.githubusercontent.com/61035602/206095792-ae0ac107-cd8e-492a-8baa-32118fc82b04.gif' height="126px" width="180px"> | <img src='https://user-images.githubusercontent.com/61035602/206095778-fdd73e5d-9f91-48c7-9d3d-6f2e02ec3f79.gif' height="126px" width="180px"> |  <img src='https://user-images.githubusercontent.com/61035602/206095709-2c3a209e-6626-45dd-be16-7f0bf4d48a14.gif' height="126px" width="180px">  | <img src="https://user-images.githubusercontent.com/61035602/206113351-cc59df79-8672-4d76-b521-a15acf69ae78.gif" height="126px" width="180px"> |

同时，PaddleDetection提供了模型的在线体验功能，用户可以选择自己的数据进行在线推理。

`说明`：考虑到服务器负载压力，在线推理均为CPU推理，完整的模型开发实例以及产业部署实践代码示例请前往[🎗️产业特色模型|产业工具](#️产业特色模型产业工具-1)。

`传送门`：[模型在线体验](https://www.paddlepaddle.org.cn/models)

<div align="center">
<p align="center">
  <img src="https://user-images.githubusercontent.com/61035602/206896755-bd0cd498-1149-4e94-ae30-da590ea78a7a.gif" align="middle"/>
</p>
</div>

## 📣最新进展

**🔥超越YOLOv8，飞桨推出精度最高的实时检测器RT-DETR！**

  <div align="center">
  <img src="https://user-images.githubusercontent.com/61035602/234011700-94de22e9-bdf9-4ce2-972d-6e6b800889c8.png"  height = "250" caption='' />
  <p></p>
  </div>

  - `RT-DETR解读文章传送门`：
    -  [《超越YOLOv8，飞桨推出精度最高的实时检测器RT-DETR！》](https://mp.weixin.qq.com/s/o03QM2rZNjHVto36gcV0Yw)
  - `代码传送门`：[RT-DETR](https://github.com/PaddlePaddle/PaddleDetection/tree/develop/configs/rtdetr)

## 👫开源社区

- **📑项目合作：** 如果您是企业开发者且有明确的目标检测垂类应用需求，请扫描如下二维码入群，并联系`群管理员AI`后可免费与官方团队展开不同层次的合作。
- **🏅️社区贡献：** PaddleDetection非常欢迎你加入到飞桨社区的开源建设中，参与贡献方式可以参考[开源项目开发指南](docs/contribution/README.md)。
- **💻直播教程：** PaddleDetection会定期在飞桨直播间([B站:飞桨PaddlePaddle](https://space.bilibili.com/476867757)、[微信: 飞桨PaddlePaddle](https://mp.weixin.qq.com/s/6ji89VKqoXDY6SSGkxS8NQ))，针对发新内容、以及产业范例、使用教程等进行直播分享。
- **🎁加入社区：** **微信扫描二维码并填写问卷之后，可以及时获取如下信息，包括：**
  - 社区最新文章、直播课等活动预告
  - 往期直播录播&PPT
  - 30+行人车辆等垂类高性能预训练模型
  - 七大任务开源数据集下载链接汇总
  - 40+前沿检测领域顶会算法
  - 15+从零上手目标检测理论与实践视频课程
  - 10+工业安防交通全流程项目实操（含源码）

<div align="center">
<img src="https://user-images.githubusercontent.com/61035602/220354166-f6bf1b5f-3038-4e8b-a196-d198ba7dce36.png"  width = "150" height = "150",caption='' />
<p>PaddleDetection官方交流群二维码</p>
</div>

- **🎈社区近期活动**
  - **🔥PaddleDetection v2.6版本更新解读**

    <div align="center">
    <img src="https://user-images.githubusercontent.com/61035602/224244188-da8495fc-eea9-432f-bc2d-6f0144c2dde9.png"  height = "250" caption='' />
    <p></p>
    </div>

    - `v2.6版本版本更新解读文章传送门`：[《PaddleDetection v2.6发布：目标小？数据缺？标注累？泛化差？PP新员逐一应对！》](https://mp.weixin.qq.com/s/SLITj5k120d_fQc7jEO8Vw)

  - **🏆半监督检测**

    - `文章传送门`：[CVPR 2023 | 单阶段半监督目标检测SOTA：ARSL](https://mp.weixin.qq.com/s/UZLIGL6va2KBfofC-nKG4g)
    - `代码传送门`：[ARSL](https://github.com/PaddlePaddle/PaddleDetection/tree/develop/configs/semi_det)

    <div align="center">
    <img src="https://user-images.githubusercontent.com/61035602/230522850-21873665-ba79-4f8d-8dce-43d736111df8.png"  height = "250" caption='' />
    <p></p>
    </div>

  - **👀YOLO系列专题**

    - `文章传送门`：[YOLOv8来啦！YOLO内卷期模型怎么选？9+款AI硬件如何快速部署？深度解析](https://mp.weixin.qq.com/s/rPwprZeHEpmGOe5wxrmO5g)
    - `代码传送门`：[PaddleYOLO全系列](https://github.com/PaddlePaddle/PaddleDetection/blob/release/2.5/docs/feature_models/PaddleYOLO_MODEL.md)

    <div align="center">
    <img src="https://user-images.githubusercontent.com/61035602/213202797-3a1b24f3-53c0-4094-bb31-db2f84438fbc.jpeg"  height = "250" caption='' />
    <p></p>
    </div>

  - **🎯少目标迁移学习专题**
    - `文章传送门`：[囿于数据少？泛化性差？PaddleDetection少样本迁移学习助你一键突围！](https://mp.weixin.qq.com/s/dFEQoxSzVCOaWVZPb3N7WA)

  - **⚽️2022卡塔尔世界杯专题**
    - `文章传送门`：[世界杯决赛号角吹响！趁周末来搭一套足球3D+AI量化分析系统吧！](https://mp.weixin.qq.com/s/koJxjWDPBOlqgI-98UsfKQ)

    <div align="center">
    <img src="https://user-images.githubusercontent.com/61035602/208036574-f151a7ff-a5f1-4495-9316-a47218a6576b.gif"  height = "250" caption='' />
    <p></p>
    </div>

  - **🔍旋转框小目标检测专题**
    - `文章传送门`：[Yes, PP-YOLOE！80.73mAP、38.5mAP，旋转框、小目标检测能力双SOTA！](https://mp.weixin.qq.com/s/6ji89VKqoXDY6SSGkxS8NQ)

    <div align="center">
    <img src="https://user-images.githubusercontent.com/61035602/208037368-5b9f01f7-afd9-46d8-bc80-271ccb5db7bb.png"  height = "220" caption='' />
    <p></p>
    </div>

  - **🎊YOLO Vision世界学术交流大会**
    - **PaddleDetection**受邀参与首个以**YOLO为主题**的**YOLO-VISION**世界大会，与全球AI领先开发者学习交流。
    - `活动链接传送门`：[YOLO-VISION](https://ultralytics.com/yolo-vision)

    <div  align="center">
    <img src="https://user-images.githubusercontent.com/48054808/192301374-940cf2fa-9661-419b-9c46-18a4570df381.jpeg" width="400"/>
    </div>

- **🏅️社区贡献**
  - `活动链接传送门`：[Yes, PP-YOLOE! 基于PP-YOLOE的算法开发](https://github.com/PaddlePaddle/PaddleDetection/issues/7345)

## ✨主要特性

#### 🧩模块化设计
PaddleDetection将检测模型解耦成不同的模块组件，通过自定义模块组件组合，用户可以便捷高效地完成检测模型的搭建。`传送门`：[🧩模块组件](#模块组件)。

#### 📱丰富的模型库
PaddleDetection支持大量的最新主流的算法基准以及预训练模型，涵盖2D/3D目标检测、实例分割、人脸检测、关键点检测、多目标跟踪、半监督学习等方向。`传送门`：[📱模型库](#模型库)、[⚖️模型性能对比](#️模型性能对比)。

#### 🎗️产业特色模型|产业工具
PaddleDetection打造产业级特色模型以及分析工具：PP-YOLOE+、PP-PicoDet、PP-TinyPose、PP-HumanV2、PP-Vehicle等，针对通用、高频垂类应用场景提供深度优化解决方案以及高度集成的分析工具，降低开发者的试错、选择成本，针对业务场景快速应用落地。`传送门`：[🎗️产业特色模型|产业工具](#️产业特色模型产业工具-1)。

#### 💡🏆产业级部署实践
PaddleDetection整理工业、农业、林业、交通、医疗、金融、能源电力等AI应用范例，打通数据标注-模型训练-模型调优-预测部署全流程，持续降低目标检测技术产业落地门槛。`传送门`：[💡产业实践范例](#产业实践范例)、[🏆企业应用案例](#企业应用案例)。

<div align="center">
<p align="center">
  <img src="https://user-images.githubusercontent.com/61035602/206431371-912a14c8-ce1e-48ec-ae6f-7267016b308e.png" align="middle" width="1280"/>
</p>
</div>


## 🍱安装

参考[安装说明](docs/tutorials/INSTALL_cn.md)进行安装。

## 🔥教程

**深度学习入门教程**

- [零基础入门深度学习](https://www.paddlepaddle.org.cn/tutorials/projectdetail/4676538)
- [零基础入门目标检测](https://aistudio.baidu.com/aistudio/education/group/info/1617)

**快速开始**

- [快速体验](docs/tutorials/QUICK_STARTED_cn.md)
- [示例：30分钟快速开发交通标志检测模型](docs/tutorials/GETTING_STARTED_cn.md)

**数据准备**
- [数据准备](docs/tutorials/data/README.md)
- [数据处理模块](docs/advanced_tutorials/READER.md)

**配置文件说明**
- [RCNN参数说明](docs/tutorials/config_annotation/faster_rcnn_r50_fpn_1x_coco_annotation.md)
- [PP-YOLO参数说明](docs/tutorials/config_annotation/ppyolo_r50vd_dcn_1x_coco_annotation.md)

**模型开发**

- [新增检测模型](docs/advanced_tutorials/MODEL_TECHNICAL.md)
- 二次开发
  - [目标检测](docs/advanced_tutorials/customization/detection.md)
  - [关键点检测](docs/advanced_tutorials/customization/keypoint_detection.md)
  - [多目标跟踪](docs/advanced_tutorials/customization/pphuman_mot.md)
  - [行为识别](docs/advanced_tutorials/customization/action_recognotion/)
  - [属性识别](docs/advanced_tutorials/customization/pphuman_attribute.md)

**部署推理**

- [模型导出教程](deploy/EXPORT_MODEL.md)
- [模型压缩](https://github.com/PaddlePaddle/PaddleSlim)
  - [剪裁/量化/蒸馏教程](configs/slim)
- [Paddle Inference部署](deploy/README.md)
  - [Python端推理部署](deploy/python)
  - [C++端推理部署](deploy/cpp)
- [Paddle Lite部署](deploy/lite)
- [Paddle Serving部署](deploy/serving)
- [ONNX模型导出](deploy/EXPORT_ONNX_MODEL.md)
- [推理benchmark](deploy/BENCHMARK_INFER.md)

## 🔑FAQ
- [FAQ/常见问题汇总](docs/tutorials/FAQ)

## 🧩模块组件

<table align="center">
  <tbody>
    <tr align="center" valign="center">
      <td>
        <b>Backbones</b>
      </td>
      <td>
        <b>Necks</b>
      </td>
      <td>
        <b>Loss</b>
      </td>
      <td>
        <b>Common</b>
      </td>
      <td>
      <b>Data Augmentation</b>
      </td>
    </tr>
    <tr valign="top">
      <td>
      <ul>
          <li><a href="ppdet/modeling/backbones/resnet.py">ResNet</a></li>
          <li><a href="ppdet/modeling/backbones/res2net.py">CSPResNet</a></li>
          <li><a href="ppdet/modeling/backbones/senet.py">SENet</a></li>
          <li><a href="ppdet/modeling/backbones/res2net.py">Res2Net</a></li>
          <li><a href="ppdet/modeling/backbones/hrnet.py">HRNet</a></li>
          <li><a href="ppdet/modeling/backbones/lite_hrnet.py">Lite-HRNet</a></li>
          <li><a href="ppdet/modeling/backbones/darknet.py">DarkNet</a></li>
          <li><a href="ppdet/modeling/backbones/csp_darknet.py">CSPDarkNet</a></li>
          <li><a href="ppdet/modeling/backbones/mobilenet_v1.py">MobileNetV1</a></li>
          <li><a href="ppdet/modeling/backbones/mobilenet_v3.py">MobileNetV1</a></li>  
          <li><a href="ppdet/modeling/backbones/shufflenet_v2.py">ShuffleNetV2</a></li>
          <li><a href="ppdet/modeling/backbones/ghostnet.py">GhostNet</a></li>
          <li><a href="ppdet/modeling/backbones/blazenet.py">BlazeNet</a></li>
          <li><a href="ppdet/modeling/backbones/dla.py">DLA</a></li>
          <li><a href="ppdet/modeling/backbones/hardnet.py">HardNet</a></li>
          <li><a href="ppdet/modeling/backbones/lcnet.py">LCNet</a></li>  
          <li><a href="ppdet/modeling/backbones/esnet.py">ESNet</a></li>  
          <li><a href="ppdet/modeling/backbones/swin_transformer.py">Swin-Transformer</a></li>
          <li><a href="ppdet/modeling/backbones/convnext.py">ConvNeXt</a></li>
          <li><a href="ppdet/modeling/backbones/vgg.py">VGG</a></li>
          <li><a href="ppdet/modeling/backbones/vision_transformer.py">Vision Transformer</a></li>
          <li><a href="configs/convnext">ConvNext</a></li>
      </ul>
      </td>
      <td>
      <ul>
        <li><a href="ppdet/modeling/necks/bifpn.py">BiFPN</a></li>
        <li><a href="ppdet/modeling/necks/blazeface_fpn.py">BlazeFace-FPN</a></li>
        <li><a href="ppdet/modeling/necks/centernet_fpn.py">CenterNet-FPN</a></li>
        <li><a href="ppdet/modeling/necks/csp_pan.py">CSP-PAN</a></li>
        <li><a href="ppdet/modeling/necks/custom_pan.py">Custom-PAN</a></li>
        <li><a href="ppdet/modeling/necks/fpn.py">FPN</a></li>
        <li><a href="ppdet/modeling/necks/es_pan.py">ES-PAN</a></li>
        <li><a href="ppdet/modeling/necks/hrfpn.py">HRFPN</a></li>
        <li><a href="ppdet/modeling/necks/lc_pan.py">LC-PAN</a></li>
        <li><a href="ppdet/modeling/necks/ttf_fpn.py">TTF-FPN</a></li>
        <li><a href="ppdet/modeling/necks/yolo_fpn.py">YOLO-FPN</a></li>
      </ul>
      </td>
      <td>
        <ul>
          <li><a href="ppdet/modeling/losses/smooth_l1_loss.py">Smooth-L1</a></li>
          <li><a href="ppdet/modeling/losses/detr_loss.py">Detr Loss</a></li>
          <li><a href="ppdet/modeling/losses/fairmot_loss.py">Fairmot Loss</a></li>
          <li><a href="ppdet/modeling/losses/fcos_loss.py">Fcos Loss</a></li>
          <li><a href="ppdet/modeling/losses/gfocal_loss.py">GFocal Loss</a></li>
          <li><a href="ppdet/modeling/losses/jde_loss.py">JDE Loss</a></li>
          <li><a href="ppdet/modeling/losses/keypoint_loss.py">KeyPoint Loss</a></li>
          <li><a href="ppdet/modeling/losses/solov2_loss.py">SoloV2 Loss</a></li>
          <li><a href="ppdet/modeling/losses/focal_loss.py">Focal Loss</a></li>
          <li><a href="ppdet/modeling/losses/iou_loss.py">GIoU/DIoU/CIoU</a></li>  
          <li><a href="ppdet/modeling/losses/iou_aware_loss.py">IoUAware</a></li>
          <li><a href="ppdet/modeling/losses/sparsercnn_loss.py">SparseRCNN Loss</a></li>
          <li><a href="ppdet/modeling/losses/ssd_loss.py">SSD Loss</a></li>
          <li><a href="ppdet/modeling/losses/focal_loss.py">YOLO Loss</a></li>
          <li><a href="ppdet/modeling/losses/yolo_loss.py">CT Focal Loss</a></li>
          <li><a href="ppdet/modeling/losses/varifocal_loss.py">VariFocal Loss</a></li>
        </ul>
      </td>
      <td>
      </ul>
          <li><b>Post-processing</b></li>
        <ul>
        <ul>
           <li><a href="ppdet/modeling/post_process.py">SoftNMS</a></li>
            <li><a href="ppdet/modeling/post_process.py">MatrixNMS</a></li>
            </ul>
            </ul>
          <li><b>Training</b></li>
        <ul>
        <ul>
            <li><a href="tools/train.py#L62">FP16 training</a></li>
            <li><a href="docs/tutorials/DistributedTraining_cn.md">Multi-machine training </a></li>
                        </ul>
            </ul>
          <li><b>Common</b></li>
        <ul>
        <ul>
            <li><a href="ppdet/modeling/backbones/resnet.py#L41">Sync-BN</a></li>
            <li><a href="configs/gn/README.md">Group Norm</a></li>
            <li><a href="configs/dcn/README.md">DCNv2</a></li>
            <li><a href="ppdet/optimizer/ema.py">EMA</a></li>
        </ul>
      </td>
      <td>
        <ul>
          <li><a href="ppdet/data/transform/operators.py">Resize</a></li>  
          <li><a href="ppdet/data/transform/operators.py">Lighting</a></li>  
          <li><a href="ppdet/data/transform/operators.py">Flipping</a></li>  
          <li><a href="ppdet/data/transform/operators.py">Expand</a></li>
          <li><a href="ppdet/data/transform/operators.py">Crop</a></li>
          <li><a href="ppdet/data/transform/operators.py">Color Distort</a></li>  
          <li><a href="ppdet/data/transform/operators.py">Random Erasing</a></li>  
          <li><a href="ppdet/data/transform/operators.py">Mixup </a></li>
          <li><a href="ppdet/data/transform/operators.py">AugmentHSV</a></li>
          <li><a href="ppdet/data/transform/operators.py">Mosaic</a></li>
          <li><a href="ppdet/data/transform/operators.py">Cutmix </a></li>
          <li><a href="ppdet/data/transform/operators.py">Grid Mask</a></li>
          <li><a href="ppdet/data/transform/operators.py">Auto Augment</a></li>  
          <li><a href="ppdet/data/transform/operators.py">Random Perspective</a></li>  
        </ul>
      </td>
    </tr>
</td>
    </tr>
  </tbody>
</table>

## 📱模型库

<table align="center">
  <tbody>
    <tr align="center" valign="center">
      <td>
        <b>2D Detection</b>
      </td>
      <td>
        <b>Multi Object Tracking</b>
      </td>
      <td>
        <b>KeyPoint Detection</b>
      </td>
      <td>
      <b>Others</b>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <ul>
            <li><a href="configs/faster_rcnn/README.md">Faster RCNN</a></li>
            <li><a href="ppdet/modeling/necks/fpn.py">FPN</a></li>
            <li><a href="configs/cascade_rcnn/README.md">Cascade-RCNN</a></li>
            <li><a href="configs/rcnn_enhance">PSS-Det</a></li>
            <li><a href="configs/retinanet/README.md">RetinaNet</a></li>
            <li><a href="configs/yolov3/README.md">YOLOv3</a></li>  
            <li><a href="configs/yolof/README.md">YOLOF</a></li>  
            <li><a href="configs/yolox/README.md">YOLOX</a></li>  
            <li><a href="https://github.com/PaddlePaddle/PaddleYOLO/tree/develop/configs/yolov5">YOLOv5</a></li>
            <li><a href="https://github.com/PaddlePaddle/PaddleYOLO/tree/develop/configs/yolov6">YOLOv6</a></li>  
            <li><a href="https://github.com/PaddlePaddle/PaddleYOLO/tree/develop/configs/yolov7">YOLOv7</a></li>
            <li><a href="https://github.com/PaddlePaddle/PaddleYOLO/tree/develop/configs/yolov8">YOLOv8</a></li>
            <li><a href="https://github.com/PaddlePaddle/PaddleYOLO/tree/develop/configs/rtmdet">RTMDet</a></li>
            <li><a href="configs/ppyolo/README_cn.md">PP-YOLO</a></li>
            <li><a href="configs/ppyolo#pp-yolo-tiny">PP-YOLO-Tiny</a></li>
            <li><a href="configs/picodet">PP-PicoDet</a></li>
            <li><a href="configs/ppyolo/README_cn.md">PP-YOLOv2</a></li>
            <li><a href="configs/ppyoloe/README_legacy.md">PP-YOLOE</a></li>
            <li><a href="configs/ppyoloe/README_cn.md">PP-YOLOE+</a></li>
            <li><a href="configs/smalldet">PP-YOLOE-SOD</a></li>
            <li><a href="configs/rotate/README.md">PP-YOLOE-R</a></li>
            <li><a href="configs/ssd/README.md">SSD</a></li>
            <li><a href="configs/centernet">CenterNet</a></li>
            <li><a href="configs/fcos">FCOS</a></li>  
            <li><a href="configs/rotate/fcosr">FCOSR</a></li>  
            <li><a href="configs/ttfnet">TTFNet</a></li>
            <li><a href="configs/tood">TOOD</a></li>
            <li><a href="configs/gfl">GFL</a></li>
            <li><a href="configs/gfl/gflv2_r50_fpn_1x_coco.yml">GFLv2</a></li>
            <li><a href="configs/detr">DETR</a></li>
            <li><a href="configs/deformable_detr">Deformable DETR</a></li>
            <li><a href="configs/sparse_rcnn">Sparse RCNN</a></li>
      </ul>
      </td>
      <td>
        <ul>
           <li><a href="configs/mot/jde">JDE</a></li>
            <li><a href="configs/mot/fairmot">FairMOT</a></li>
            <li><a href="configs/mot/deepsort">DeepSORT</a></li>
            <li><a href="configs/mot/bytetrack">ByteTrack</a></li>
            <li><a href="configs/mot/ocsort">OC-SORT</a></li>
            <li><a href="configs/mot/botsort">BoT-SORT</a></li>
            <li><a href="configs/mot/centertrack">CenterTrack</a></li>
        </ul>
      </td>
      <td>
        <ul>
          <li><a href="configs/keypoint/hrnet">HRNet</a></li>
            <li><a href="configs/keypoint/higherhrnet">HigherHRNet</a></li>
            <li><a href="configs/keypoint/lite_hrnet">Lite-HRNet</a></li>
            <li><a href="configs/keypoint/tiny_pose">PP-TinyPose</a></li>
        </ul>
</td>
<td>
</ul>
          <li><b>Instance Segmentation</b></li>
        <ul>
        <ul>
          <li><a href="configs/mask_rcnn">Mask RCNN</a></li>
            <li><a href="configs/cascade_rcnn">Cascade Mask RCNN</a></li>
            <li><a href="configs/solov2">SOLOv2</a></li>
        </ul>
      </ul>
          <li><b>Face Detection</b></li>
        <ul>
        <ul>
            <li><a href="configs/face_detection">BlazeFace</a></li>
        </ul>
        </ul>
          <li><b>Semi-Supervised Detection</b></li>
        <ul>
        <ul>
            <li><a href="configs/semi_det">DenseTeacher</a></li>
        </ul>
        </ul>
          <li><b>3D Detection</b></li>
        <ul>
        <ul>
            <li><a href="https://github.com/PaddlePaddle/Paddle3D">Smoke</a></li>
            <li><a href="https://github.com/PaddlePaddle/Paddle3D">CaDDN</a></li>
            <li><a href="https://github.com/PaddlePaddle/Paddle3D">PointPillars</a></li>
            <li><a href="https://github.com/PaddlePaddle/Paddle3D">CenterPoint</a></li>
            <li><a href="https://github.com/PaddlePaddle/Paddle3D">SequeezeSegV3</a></li>
            <li><a href="https://github.com/PaddlePaddle/Paddle3D">IA-SSD</a></li>
            <li><a href="https://github.com/PaddlePaddle/Paddle3D">PETR</a></li>
        </ul>
        </ul>
          <li><b>Vehicle Analysis Toolbox</b></li>
        <ul>
        <ul>
            <li><a href="deploy/pipeline/README.md">PP-Vehicle</a></li>
        </ul>
        </ul>
          <li><b>Human Analysis Toolbox</b></li>
        <ul>
        <ul>
            <li><a href="deploy/pipeline/README.md">PP-Human</a></li>
            <li><a href="deploy/pipeline/README.md">PP-HumanV2</a></li>
        </ul>
        </ul>
          <li><b>Sport Analysis Toolbox</b></li>
        <ul>
        <ul>
            <li><a href="https://github.com/PaddlePaddle/PaddleSports">PP-Sports</a></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

## ⚖️模型性能对比

#### 🖥️服务器端模型性能对比

各模型结构和骨干网络的代表模型在COCO数据集上精度mAP和单卡Tesla V100上预测速度(FPS)对比图。

  <div  align="center">
  <img src="https://user-images.githubusercontent.com/61035602/206434766-caaa781b-b922-481f-af09-15faac9ed33b.png" width="800"/>
</div>

<details>
<summary><b> 测试说明(点击展开)</b></summary>

- ViT为ViT-Cascade-Faster-RCNN模型，COCO数据集mAP高达55.7%
- Cascade-Faster-RCNN为Cascade-Faster-RCNN-ResNet50vd-DCN，PaddleDetection将其优化到COCO数据mAP为47.8%时推理速度为20FPS
- PP-YOLOE是对PP-YOLO v2模型的进一步优化，L版本在COCO数据集mAP为51.6%，Tesla V100预测速度78.1FPS
- PP-YOLOE+是对PPOLOE模型的进一步优化，L版本在COCO数据集mAP为53.3%，Tesla V100预测速度78.1FPS
- YOLOX和YOLOv5均为基于PaddleDetection复现算法，YOLOv5代码在[PaddleYOLO](https://github.com/PaddlePaddle/PaddleYOLO)中，参照[PaddleYOLO_MODEL](docs/feature_models/PaddleYOLO_MODEL.md)
- 图中模型均可在[📱模型库](#模型库)中获取
</details>

#### ⌚️移动端模型性能对比

各移动端模型在COCO数据集上精度mAP和高通骁龙865处理器上预测速度(FPS)对比图。

  <div  align="center">
  <img src="https://user-images.githubusercontent.com/61035602/206434741-10460690-8fc3-4084-a11a-16fe4ce2fc85.png" width="550"/>
</div>


<details>
<summary><b> 测试说明(点击展开)</b></summary>

- 测试数据均使用高通骁龙865(4xA77+4xA55)处理器，batch size为1, 开启4线程测试，测试使用NCNN预测库，测试脚本见[MobileDetBenchmark](https://github.com/JiweiMaster/MobileDetBenchmark)
- PP-PicoDet及PP-YOLO-Tiny为PaddleDetection自研模型，可在[📱模型库](#模型库)中获取，其余模型PaddleDetection暂未提供
</details>

## 🎗️产业特色模型|产业工具

产业特色模型｜产业工具是PaddleDetection针对产业高频应用场景打造的兼顾精度和速度的模型以及工具箱，注重从数据处理-模型训练-模型调优-模型部署的端到端打通，且提供了实际生产环境中的实践范例代码，帮助拥有类似需求的开发者高效的完成产品开发落地应用。

该系列模型｜工具均已PP前缀命名，具体介绍、预训练模型以及产业实践范例代码如下。

### 💎PP-YOLOE 高精度目标检测模型

<details>
<summary><b> 简介(点击展开)</b></summary>

PP-YOLOE是基于PP-YOLOv2的卓越的单阶段Anchor-free模型，超越了多种流行的YOLO模型。PP-YOLOE避免了使用诸如Deformable Convolution或者Matrix NMS之类的特殊算子，以使其能轻松地部署在多种多样的硬件上。其使用大规模数据集obj365预训练模型进行预训练，可以在不同场景数据集上快速调优收敛。

`传送门`：[PP-YOLOE说明](configs/ppyoloe/README_cn.md)。

`传送门`：[arXiv论文](https://arxiv.org/abs/2203.16250)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>

| 模型名称    | COCO精度（mAP） | V100 TensorRT FP16速度(FPS) | 推荐部署硬件 |                        配置文件                         |                                        模型下载                                         |
| :---------- | :-------------: | :-------------------------: | :----------: | :-----------------------------------------------------: | :-------------------------------------------------------------------------------------: |
| PP-YOLOE+_l |      53.3       |            149.2            |    服务器    | [链接](configs/ppyoloe/ppyoloe_plus_crn_l_80e_coco.yml) | [下载地址](https://paddledet.bj.bcebos.com/models/ppyoloe_plus_crn_m_80e_coco.pdparams) |

`传送门`：[全部预训练模型](configs/ppyoloe/README_cn.md)。
</details>

<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业 | 类别              | 亮点                                                                                          | 文档说明                                                      | 模型下载                                            |
| ---- | ----------------- | --------------------------------------------------------------------------------------------- | ------------------------------------------------------------- | --------------------------------------------------- |
| 农业 | 农作物检测        | 用于葡萄栽培中基于图像的监测和现场机器人技术，提供了来自5种不同葡萄品种的实地实例             | [PP-YOLOE+ 下游任务](./configs/ppyoloe/application/README.md) | [下载链接](./configs/ppyoloe/application/README.md) |
| 通用 | 低光场景检测      | 低光数据集使用ExDark，包括从极低光环境到暮光环境等10种不同光照条件下的图片。                  | [PP-YOLOE+ 下游任务](./configs/ppyoloe/application/README.md) | [下载链接](./configs/ppyoloe/application/README.md) |
| 工业 | PCB电路板瑕疵检测 | 工业数据集使用PKU-Market-PCB，该数据集用于印刷电路板（PCB）的瑕疵检测，提供了6种常见的PCB缺陷 | [PP-YOLOE+ 下游任务](./configs/ppyoloe/application/README.md) | [下载链接](./configs/ppyoloe/application/README.md) |
</details>

### 💎PP-YOLOE-R 高性能旋转框检测模型

<details>
<summary><b> 简介(点击展开)</b></summary>

PP-YOLOE-R是一个高效的单阶段Anchor-free旋转框检测模型，基于PP-YOLOE+引入了一系列改进策略来提升检测精度。根据不同的硬件对精度和速度的要求，PP-YOLOE-R包含s/m/l/x四个尺寸的模型。在DOTA 1.0数据集上，PP-YOLOE-R-l和PP-YOLOE-R-x在单尺度训练和测试的情况下分别达到了78.14mAP和78.28 mAP，这在单尺度评估下超越了几乎所有的旋转框检测模型。通过多尺度训练和测试，PP-YOLOE-R-l和PP-YOLOE-R-x的检测精度进一步提升至80.02mAP和80.73 mAP，超越了所有的Anchor-free方法并且和最先进的Anchor-based的两阶段模型精度几乎相当。在保持高精度的同时，PP-YOLOE-R避免使用特殊的算子，例如Deformable Convolution或Rotated RoI Align，使其能轻松地部署在多种多样的硬件上。

`传送门`：[PP-YOLOE-R说明](configs/rotate/ppyoloe_r)。

`传送门`：[arXiv论文](https://arxiv.org/abs/2211.02386)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>

|     模型     | Backbone |  mAP  | V100 TRT FP16 (FPS) | RTX 2080 Ti TRT FP16 (FPS) | Params (M) | FLOPs (G) | 学习率策略 | 角度表示 | 数据增广 | GPU数目 | 每GPU图片数目 |                                      模型下载                                       |                                                            配置文件                                                            |
| :----------: | :------: | :---: | :-----------------: | :------------------------: | :--------: | :-------: | :--------: | :------: | :------: | :-----: | :-----------: | :---------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------: |
| PP-YOLOE-R-l |  CRN-l   | 80.02 |        69.7         |            48.3            |   53.29    |  281.65   |     3x     |    oc    |  MS+RR   |    4    |       2       | [model](https://paddledet.bj.bcebos.com/models/ppyoloe_r_crn_l_3x_dota_ms.pdparams) | [config](https://github.com/PaddlePaddle/PaddleDetection/tree/develop/configs/rotate/ppyoloe_r/ppyoloe_r_crn_l_3x_dota_ms.yml) |

`传送门`：[全部预训练模型](configs/rotate/ppyoloe_r)。
</details>

<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业 | 类别       | 亮点                                                                  | 文档说明                                                                                | 模型下载                                                              |
| ---- | ---------- | --------------------------------------------------------------------- | --------------------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| 通用 | 旋转框检测 | 手把手教你上手PP-YOLOE-R旋转框检测，10分钟将脊柱数据集精度训练至95mAP | [基于PP-YOLOE-R的旋转框检测](https://aistudio.baidu.com/aistudio/projectdetail/5058293) | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/5058293) |
</details>

### 💎PP-YOLOE-SOD 高精度小目标检测模型

<details>
<summary><b> 简介(点击展开)</b></summary>

PP-YOLOE-SOD(Small Object Detection)是PaddleDetection团队针对小目标检测提出的检测方案，在VisDrone-DET数据集上单模型精度达到38.5mAP，达到了SOTA性能。其分别基于切图拼图流程优化的小目标检测方案以及基于原图模型算法优化的小目标检测方案。同时提供了数据集自动分析脚本，只需输入数据集标注文件，便可得到数据集统计结果，辅助判断数据集是否是小目标数据集以及是否需要采用切图策略，同时给出网络超参数参考值。

`传送门`：[PP-YOLOE-SOD 小目标检测模型](configs/smalldet)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>
- VisDrone数据集预训练模型

| 模型                | COCOAPI mAP<sup>val<br>0.5:0.95 | COCOAPI mAP<sup>val<br>0.5 | COCOAPI mAP<sup>test_dev<br>0.5:0.95 | COCOAPI mAP<sup>test_dev<br>0.5 | MatlabAPI mAP<sup>test_dev<br>0.5:0.95 | MatlabAPI mAP<sup>test_dev<br>0.5 |                                              下载                                               |                           配置文件                           |
| :------------------ | :-----------------------------: | :------------------------: | :----------------------------------: | :-----------------------------: | :------------------------------------: | :-------------------------------: | :---------------------------------------------------------------------------------------------: | :----------------------------------------------------------: |
| **PP-YOLOE+_SOD-l** |            **31.9**             |          **52.1**          |               **25.6**               |            **43.5**             |               **30.25**                |             **51.18**             | [下载链接](https://paddledet.bj.bcebos.com/models/ppyoloe_plus_sod_crn_l_80e_visdrone.pdparams) | [配置文件](visdrone/ppyoloe_plus_sod_crn_l_80e_visdrone.yml) |

`传送门`：[全部预训练模型](configs/smalldet)。
</details>

<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业 | 类别       | 亮点                                                 | 文档说明                                                                                          | 模型下载                                                              |
| ---- | ---------- | ---------------------------------------------------- | ------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| 通用 | 小目标检测 | 基于PP-YOLOE-SOD的无人机航拍图像检测案例全流程实操。 | [基于PP-YOLOE-SOD的无人机航拍图像检测](https://aistudio.baidu.com/aistudio/projectdetail/5036782) | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/5036782) |
</details>

### 💫PP-PicoDet 超轻量实时目标检测模型

<details>
<summary><b> 简介(点击展开)</b></summary>

全新的轻量级系列模型PP-PicoDet，在移动端具有卓越的性能，成为全新SOTA轻量级模型。

`传送门`：[PP-PicoDet说明](configs/picodet/README.md)。

`传送门`：[arXiv论文](https://arxiv.org/abs/2111.00902)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>

| 模型名称  | COCO精度（mAP） | 骁龙865 四线程速度(FPS) |  推荐部署硬件  |                       配置文件                       |                                       模型下载                                       |
| :-------- | :-------------: | :---------------------: | :------------: | :--------------------------------------------------: | :----------------------------------------------------------------------------------: |
| PicoDet-L |      36.1       |          39.7           | 移动端、嵌入式 | [链接](configs/picodet/picodet_l_320_coco_lcnet.yml) | [下载地址](https://paddledet.bj.bcebos.com/models/picodet_l_320_coco_lcnet.pdparams) |

`传送门`：[全部预训练模型](configs/picodet/README.md)。
</details>


<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业     | 类别         | 亮点                                                                                                                           | 文档说明                                                                                                          | 模型下载                                                                                      |
| -------- | ------------ | ------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- |
| 智慧城市 | 道路垃圾检测 | 通过在市政环卫车辆上安装摄像头对路面垃圾检测并分析，实现对路面遗撒的垃圾进行监控，记录并通知环卫人员清理，大大提升了环卫人效。 | [基于PP-PicoDet的路面垃圾检测](https://aistudio.baidu.com/aistudio/projectdetail/3846170?channelType=0&channel=0) | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/3846170?channelType=0&channel=0) |
</details>

### 📡PP-Tracking 实时多目标跟踪系统

<details>
<summary><b> 简介(点击展开)</b></summary>

PaddleDetection团队提供了实时多目标跟踪系统PP-Tracking，是基于PaddlePaddle深度学习框架的业界首个开源的实时多目标跟踪系统，具有模型丰富、应用广泛和部署高效三大优势。 PP-Tracking支持单镜头跟踪(MOT)和跨镜头跟踪(MTMCT)两种模式，针对实际业务的难点和痛点，提供了行人跟踪、车辆跟踪、多类别跟踪、小目标跟踪、流量统计以及跨镜头跟踪等各种多目标跟踪功能和应用，部署方式支持API调用和GUI可视化界面，部署语言支持Python和C++，部署平台环境支持Linux、NVIDIA Jetson等。

`传送门`：[PP-Tracking说明](configs/mot/README.md)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>

| 模型名称  |               模型简介               |          精度          | 速度(FPS) |      推荐部署硬件      |                          配置文件                          |                                              模型下载                                              |
| :-------- | :----------------------------------: | :--------------------: | :-------: | :--------------------: | :--------------------------------------------------------: | :------------------------------------------------------------------------------------------------: |
| ByteTrack |   SDE多目标跟踪算法 仅包含检测模型   |   MOT-17 test:  78.4   |     -     | 服务器、移动端、嵌入式 |     [链接](configs/mot/bytetrack/bytetrack_yolox.yml)      |  [下载地址](https://bj.bcebos.com/v1/paddledet/models/mot/yolox_x_24e_800x1440_mix_det.pdparams)   |
| FairMOT   | JDE多目标跟踪算法 多任务联合学习方法 |   MOT-16 test: 75.0    |     -     | 服务器、移动端、嵌入式 | [链接](configs/mot/fairmot/fairmot_dla34_30e_1088x608.yml) |     [下载地址](https://paddledet.bj.bcebos.com/models/mot/fairmot_dla34_30e_1088x608.pdparams)     |
| OC-SORT   |   SDE多目标跟踪算法 仅包含检测模型   | MOT-17 half val:  75.5 |     -     | 服务器、移动端、嵌入式 |        [链接](configs/mot/ocsort/ocsort_yolox.yml)         | [下载地址](https://bj.bcebos.com/v1/paddledet/models/mot/yolox_x_24e_800x1440_mix_mot_ch.pdparams) |
</details>

<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业 | 类别       | 亮点                       | 文档说明                                                                                       | 模型下载                                                              |
| ---- | ---------- | -------------------------- | ---------------------------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| 通用 | 多目标跟踪 | 快速上手单镜头、多镜头跟踪 | [PP-Tracking之手把手玩转多目标跟踪](https://aistudio.baidu.com/aistudio/projectdetail/3022582) | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/3022582) |
</details>

### ⛷️PP-TinyPose 人体骨骼关键点识别

<details>
<summary><b> 简介(点击展开)</b></summary>

PaddleDetection 中的关键点检测部分紧跟最先进的算法，包括 Top-Down 和 Bottom-Up 两种方法，可以满足用户的不同需求。同时，PaddleDetection 提供针对移动端设备优化的自研实时关键点检测模型 PP-TinyPose。

`传送门`：[PP-TinyPose说明](configs/keypoint/tiny_pose)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>

|  模型名称   |               模型简介               | COCO精度（AP） |         速度(FPS)         |  推荐部署硬件  |                        配置文件                         |                                         模型下载                                         |
| :---------: | :----------------------------------: | :------------: | :-----------------------: | :------------: | :-----------------------------------------------------: | :--------------------------------------------------------------------------------------: |
| PP-TinyPose | 轻量级关键点算法<br/>输入尺寸256x192 |      68.8      | 骁龙865 四线程: 158.7 FPS | 移动端、嵌入式 | [链接](configs/keypoint/tiny_pose/tinypose_256x192.yml) | [下载地址](https://bj.bcebos.com/v1/paddledet/models/keypoint/tinypose_256x192.pdparams) |

`传送门`：[全部预训练模型](configs/keypoint/README.md)。
</details>

<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业 | 类别 | 亮点                                                                                                                                     | 文档说明                                                                                             | 模型下载                                                              |
| ---- | ---- | ---------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| 运动 | 健身 | 提供从模型选型、数据准备、模型训练优化，到后处理逻辑和模型部署的全流程可复用方案，有效解决了复杂健身动作的高效识别，打造AI虚拟健身教练！ | [基于PP-TinyPose增强版的智能健身动作识别](https://aistudio.baidu.com/aistudio/projectdetail/4385813) | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/4385813) |
</details>

### 🏃🏻PP-Human 实时行人分析工具

<details>
<summary><b> 简介(点击展开)</b></summary>

PaddleDetection深入探索核心行业的高频场景，提供了行人开箱即用分析工具，支持图片/单镜头视频/多镜头视频/在线视频流多种输入方式，广泛应用于智慧交通、智慧城市、工业巡检等领域。支持服务器端部署及TensorRT加速，T4服务器上可达到实时。
PP-Human支持四大产业级功能：五大异常行为识别、26种人体属性分析、实时人流计数、跨镜头（ReID）跟踪。

`传送门`：[PP-Human行人分析工具使用指南](deploy/pipeline/README.md)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>

|        任务        | T4 TensorRT FP16: 速度（FPS） | 推荐部署硬件 |                                                                                                                                         模型下载                                                                                                                                         |                             模型体积                              |
| :----------------: | :---------------------------: | :----------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------: |
| 行人检测（高精度） |             39.8              |    服务器    |                                                                                              [目标检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip)                                                                                               |                               182M                                |
| 行人跟踪（高精度） |             31.4              |    服务器    |                                                                                             [多目标跟踪](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip)                                                                                              |                               182M                                |
| 属性识别（高精度） |          单人 117.6           |    服务器    |                                      [目标检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip)<br> [属性识别](https://bj.bcebos.com/v1/paddledet/models/pipeline/PPHGNet_small_person_attribute_954_infer.zip)                                       |                  目标检测：182M<br>属性识别：86M                  |
|      摔倒识别      |           单人 100            |    服务器    | [多目标跟踪](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip) <br> [关键点检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/dark_hrnet_w32_256x192.zip) <br> [基于关键点行为识别](https://bj.bcebos.com/v1/paddledet/models/pipeline/STGCN.zip) | 多目标跟踪：182M<br>关键点检测：101M<br>基于关键点行为识别：21.8M |
|      闯入识别      |             31.4              |    服务器    |                                                                                             [多目标跟踪](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip)                                                                                              |                               182M                                |
|      打架识别      |             50.8              |    服务器    |                                                                                              [视频分类](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip)                                                                                               |                                90M                                |
|      抽烟识别      |             340.1             |    服务器    |                                    [目标检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip)<br>[基于人体id的目标检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/ppyoloe_crn_s_80e_smoking_visdrone.zip)                                    |            目标检测：182M<br>基于人体id的目标检测：27M            |
|     打电话识别     |             166.7             |    服务器    |                                      [目标检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_pipeline.zip)<br>[基于人体id的图像分类](https://bj.bcebos.com/v1/paddledet/models/pipeline/PPHGNet_tiny_calling_halfbody.zip)                                       |            目标检测：182M<br>基于人体id的图像分类：45M            |

`传送门`：[完整预训练模型](deploy/pipeline/README.md)。
</details>

<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业     | 类别     | 亮点                                                                                                                                           | 文档说明                                                                                               | 模型下载                                                                                 |
| -------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------- |
| 智能安防 | 摔倒检测 | 飞桨行人分析PP-Human中提供的摔倒识别算法，采用了关键点+时空图卷积网络的技术，对摔倒姿势无限制、背景环境无要求。                                | [基于PP-Human v2的摔倒检测](https://aistudio.baidu.com/aistudio/projectdetail/4606001)                 | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/4606001)                    |
| 智能安防 | 打架识别 | 本项目基于PaddleVideo视频开发套件训练打架识别模型，然后将训练好的模型集成到PaddleDetection的PP-Human中，助力行人行为分析。                     | [基于PP-Human的打架识别](https://aistudio.baidu.com/aistudio/projectdetail/4086987?contributionType=1) | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/4086987?contributionType=1) |
| 智能安防 | 摔倒检测 | 基于PP-Human完成来客分析整体流程。使用PP-Human完成来客分析中非常常见的场景： 1. 来客属性识别(单镜和跨境可视化）；2. 来客行为识别（摔倒识别）。 | [基于PP-Human的来客分析案例教程](https://aistudio.baidu.com/aistudio/projectdetail/4537344)            | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/4537344)                    |
</details>

### 🏎️PP-Vehicle 实时车辆分析工具

<details>
<summary><b> 简介(点击展开)</b></summary>

PaddleDetection深入探索核心行业的高频场景，提供了车辆开箱即用分析工具，支持图片/单镜头视频/多镜头视频/在线视频流多种输入方式，广泛应用于智慧交通、智慧城市、工业巡检等领域。支持服务器端部署及TensorRT加速，T4服务器上可达到实时。
PP-Vehicle囊括四大交通场景核心功能：车牌识别、属性识别、车流量统计、违章检测。

`传送门`：[PP-Vehicle车辆分析工具指南](deploy/pipeline/README.md)。

</details>

<details>
<summary><b> 预训练模型(点击展开)</b></summary>

|        任务        | T4 TensorRT FP16: 速度(FPS) | 推荐部署硬件 |                                                                                           模型方案                                                                                           |                模型体积                 |
| :----------------: | :-------------------------: | :----------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-------------------------------------: |
| 车辆检测（高精度） |            38.9             |    服务器    |                                                [目标检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_ppvehicle.zip)                                                |                  182M                   |
| 车辆跟踪（高精度） |             25              |    服务器    |                                               [多目标跟踪](https://bj.bcebos.com/v1/paddledet/models/pipeline/mot_ppyoloe_l_36e_ppvehicle.zip)                                               |                  182M                   |
|      车牌识别      |            213.7            |    服务器    | [车牌检测](https://bj.bcebos.com/v1/paddledet/models/pipeline/ch_PP-OCRv3_det_infer.tar.gz) <br> [车牌识别](https://bj.bcebos.com/v1/paddledet/models/pipeline/ch_PP-OCRv3_rec_infer.tar.gz) | 车牌检测：3.9M  <br> 车牌字符识别： 12M |
|      车辆属性      |            136.8            |    服务器    |                                                  [属性识别](https://bj.bcebos.com/v1/paddledet/models/pipeline/vehicle_attribute_model.zip)                                                  |                  7.2M                   |

`传送门`：[完整预训练模型](deploy/pipeline/README.md)。
</details>

<details>
<summary><b> 产业应用代码示例(点击展开)</b></summary>

| 行业     | 类别             | 亮点                                                                                                               | 文档说明                                                                                      | 模型下载                                                              |
| -------- | ---------------- | ------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| 智慧交通 | 交通监控车辆分析 | 本项目基于PP-Vehicle演示智慧交通中最刚需的车流量监控、车辆违停检测以及车辆结构化（车牌、车型、颜色）分析三大场景。 | [基于PP-Vehicle的交通监控分析系统](https://aistudio.baidu.com/aistudio/projectdetail/4512254) | [下载链接](https://aistudio.baidu.com/aistudio/projectdetail/4512254) |
</details>

## 💡产业实践范例

产业实践范例是PaddleDetection针对高频目标检测应用场景，提供的端到端开发示例，帮助开发者打通数据标注-模型训练-模型调优-预测部署全流程。
针对每个范例我们都通过[AI-Studio](https://ai.baidu.com/ai-doc/AISTUDIO/Tk39ty6ho)提供了项目代码以及说明，用户可以同步运行体验。

`传送门`：[产业实践范例完整列表](industrial_tutorial/README.md)

- [基于PP-YOLOE-R的旋转框检测](https://aistudio.baidu.com/aistudio/projectdetail/5058293)
- [基于PP-YOLOE-SOD的无人机航拍图像检测](https://aistudio.baidu.com/aistudio/projectdetail/5036782)
- [基于PP-Vehicle的交通监控分析系统](https://aistudio.baidu.com/aistudio/projectdetail/4512254)
- [基于PP-Human v2的摔倒检测](https://aistudio.baidu.com/aistudio/projectdetail/4606001)
- [基于PP-TinyPose增强版的智能健身动作识别](https://aistudio.baidu.com/aistudio/projectdetail/4385813)
- [基于PP-Human的打架识别](https://aistudio.baidu.com/aistudio/projectdetail/4086987?contributionType=1)
- [基于Faster-RCNN的瓷砖表面瑕疵检测](https://aistudio.baidu.com/aistudio/projectdetail/2571419)
- [基于PaddleDetection的PCB瑕疵检测](https://aistudio.baidu.com/aistudio/projectdetail/2367089)
- [基于FairMOT实现人流量统计](https://aistudio.baidu.com/aistudio/projectdetail/2421822)
- [基于YOLOv3实现跌倒检测](https://aistudio.baidu.com/aistudio/projectdetail/2500639)
- [基于PP-PicoDetv2 的路面垃圾检测](https://aistudio.baidu.com/aistudio/projectdetail/3846170?channelType=0&channel=0)
- [基于人体关键点检测的合规检测](https://aistudio.baidu.com/aistudio/projectdetail/4061642?contributionType=1)
- [基于PP-Human的来客分析案例教程](https://aistudio.baidu.com/aistudio/projectdetail/4537344)
- 持续更新中...

## 🏆企业应用案例

企业应用案例是企业在实生产环境下落地应用PaddleDetection的方案思路，相比产业实践范例其更多强调整体方案设计思路，可供开发者在项目方案设计中做参考。

`传送门`：[企业应用案例完整列表](https://www.paddlepaddle.org.cn/customercase)

- [中国南方电网——变电站智慧巡检](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2330)
- [国铁电气——轨道在线智能巡检系统](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2280)
- [京东物流——园区车辆行为识别](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2611)
- [中兴克拉—厂区传统仪表统计监测](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2618)
- [宁德时代—动力电池高精度质量检测](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2609)
- [中国科学院空天信息创新研究院——高尔夫球场遥感监测](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2483)
- [御航智能——基于边缘的无人机智能巡检](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2481)
- [普宙无人机——高精度森林巡检](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2121)
- [领邦智能——红外无感测温监控](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2615)
- [北京地铁——口罩检测](https://mp.weixin.qq.com/s/znrqaJmtA7CcjG0yQESWig)
- [音智达——工厂人员违规行为检测](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2288)
- [华夏天信——输煤皮带机器人智能巡检](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2331)
- [优恩物联网——社区住户分类支持广告精准投放](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2485)
- [螳螂慧视——室内3D点云场景物体分割与检测](https://www.paddlepaddle.org.cn/support/news?action=detail&id=2599)
- 持续更新中...

## 📝许可证书

本项目的发布受[Apache 2.0 license](LICENSE)许可认证。


## 📌引用

```
@misc{ppdet2019,
title={PaddleDetection, Object detection and instance segmentation toolkit based on PaddlePaddle.},
author={PaddlePaddle Authors},
howpublished = {\url{https://github.com/PaddlePaddle/PaddleDetection}},
year={2019}
}
```
![输入图片说明](1710146200217.png)