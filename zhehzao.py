        if visualize:
            for outs in results:
                batch_res = get_infer_results(outs, clsid2catid)
                bbox_num = outs['bbox_num']
                # logger.info(str(batch_res))

                start = 0

                for i, im_id in enumerate(outs['im_id']):
                    image_path = imid2path[int(im_id)]
                    image = Image.open(image_path).convert('RGB')
                    image = ImageOps.exif_transpose(image)
                    # 创建一个与图片大小一样的布尔矩阵，初始值全为False
                    if start == 0:
                        region_matrix = np.full_like(image, fill_value=False, dtype=bool)

                    # 显示新的图片
                    # zero_im.show()
                    self.status['original_image'] = np.array(image.copy())

                    end = start + bbox_num[i]
                    bbox_res = batch_res['bbox'][start:end] \
                            if 'bbox' in batch_res else None
                    print(type(bbox_res))

                    for i in list(bbox_res):
                        print(i)
                        print(i['bbox'])
                        print([int(x) for x in i['bbox']])
                        li=[int(x) for x in i['bbox']]
                        if li[0]>li[2] :
                            li[0], li[2] = li[2], li[0]
                        if li[1]>li[3] :
                            li[1], li[3] = li[3], li[1]
                        
                        # 将图片转换为NumPy数组
                        # image_array = np.array(zero_im)

                        # 定义你的区域坐标
                        x1, y1, x2, y2 = [int(coord) for coord in li]

                        # 将指定区域的像素值设置为True
                        region_matrix[y1:y2, x1:x2] = True

                        # 将布尔矩阵转换为uint8矩阵，以便与原始图片的像素值兼容
                        region_matrix1 = region_matrix.astype(np.uint8) * 255

                        # 将修改后的数组转换回图片
                        new_image = Image.fromarray(region_matrix1)

                        # 显示新的图片
                        new_image.show()